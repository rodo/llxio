# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
llxio views
"""
import json
import urllib
from django.conf import settings
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import redirect, get_object_or_404
from django.db.models import F
from django.views.decorators.cache import cache_page
from llxio.link import models


@cache_page(300)
def home(request):
   """Home page
   """
   try:
       purl = urllib.unquote(request.GET['url'])       
   except:
       purl = None

   if purl is not None:
      if not purl.startswith('http://lolix.org/'):
         raise Http404

      url = purl[16:]
      links = models.Link.objects.filter(link=url)

      if len(links) == 0:
         link_db = models.Link()
         link_db.link = url
         link_db.save()
      else:
         link_db = links[0]

      short_url = link_db.get_short_id()
      datas = {"short_url": short_url}
      return HttpResponse(json.dumps(datas),
                          mimetype='application/json')
   else:
      links = []
      db_links = models.Link.objects.all().order_by('-pk')[:6]

      for link in db_links:
         links.append({'url': "lolix.org{}".format(link.link),
                       'url_full': "http://lolix.org{}".format(link.link),
                       'short': "llx.io/{}".format(link.get_short_id()),
                       'short_full': "http://llx.io/{}".format(link.get_short_id())                      })


      return render_to_response("index.html",
                                {'links': links},
                                context_instance=RequestContext(request))



@cache_page(3600)
def link(request, id):
   db_id = models.Link.decode_id(id)
   link_db = get_object_or_404(models.Link, id=db_id)
   models.Link.objects.filter(id=db_id).update(hits=F('hits')+1)
   return redirect("{}{}".format(settings.REDIRECT_DOMAIN,
                                 link_db.link))

